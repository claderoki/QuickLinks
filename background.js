function search(nameKey, myArray)
{
    for (var i=0; i < myArray.length; i++) 
    {
        if (myArray[i]["url"] === nameKey) 
        {
            return myArray[i];
        }
    }
}

browser.contextMenus.create(
{
    id: "quick-link",
    title: "Quick Link",
    contexts: ["selection"]
});

browser.storage.local.get().then((data) =>
{ 
    if (Object.keys(data).length === 0) 
    {  // if nothing in local storage, set default settings, and set "selections" as the newly created default settings.
        let selectionsObject = {"selections":[{"url": "https://translate.google.com/#auto/fr/%selection%","name":"Google Translate (auto-FR)"}]}
        browser.storage.local.set({selectionsObject}).then(null);
        var selections = selectionsObject["selections"]
    }
    else
    { // if local storage isn't empty, set selections as the local storage data.
        var selections = data["selectionsObject"]["selections"]
    }

    for (index in selections)
    { // loop through all the selections in local storage or default settings and create context menu's under the quick-link menu.
        browser.contextMenus.create(
        {
            parentId: "quick-link",
            title: selections[index]["name"],
            id: selections[index]["url"],
        });
    }
});




browser.contextMenus.onClicked.addListener((info, tab) => 
{
    if (info.parentMenuItemId === "quick-link")
    {
        browser.storage.local.get().then((data) =>
        {
            var selection = search(info.menuItemId, data["selectionsObject"]["selections"]);
            browser.tabs.create({url: selection["url"].replace("%selection%",info.selectionText)});
        });
    }
});