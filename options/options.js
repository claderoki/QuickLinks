table = document.getElementById("main-table");

browser.storage.local.get().then((data) =>
{
    var row = table.insertRow();
    let nameHeaderCell = row.insertCell();
    let urlHeaderCell= row.insertCell();

    nameHeaderCell.appendChild(document.createTextNode("name"));
    urlHeaderCell.appendChild(document.createTextNode("url"));

    for (let x = 0; data["selectionsObject"]["selections"].length > x; x++)
    {   
        var selection = data["selectionsObject"]["selections"][x];
        var row = table.insertRow();
        row.id = selection["url"];

        var nameInputCell = row.insertCell();
        var urlInputCell = row.insertCell();
        var removeCellButton = row.insertCell();

        var nameInput = document.createElement("input");
        nameInput.setAttribute('type',"text");
        nameInput.setAttribute('value',selection["name"]);
        nameInput.setAttribute('name',"name");

        var urlInput = document.createElement("input");
        urlInput.setAttribute('type',"text");
        urlInput.setAttribute('value',selection["url"]);
        urlInput.setAttribute('name',"url");

        var removeButton = document.createElement("input");
        removeButton.setAttribute('type',"button");
        removeButton.setAttribute('value',"Remove");

        nameInputCell.appendChild(nameInput);
        urlInputCell.appendChild(urlInput);
        removeCellButton.appendChild(removeButton);
    }


    let addFieldButton = document.createElement("input");
    addFieldButton.setAttribute('type',"button");
    addFieldButton.setAttribute('value',"Add Field");
    document.body.appendChild(addFieldButton);

    let saveButton = document.createElement("input");
    saveButton.setAttribute('type',"button");
    saveButton.setAttribute('value',"Save");
    document.body.appendChild(saveButton);

    document.body.appendChild(table);

});

document.addEventListener("keyup",(e)=>
{
    if (e.target.getAttribute("name") === "url")
    {
        if (e.target.parentElement.previousSibling.firstChild.value)
        { 
            e.target.parentElement.parentElement.setAttribute("id",e.target.value);
        }
    }
});

document.addEventListener("click", (e) => 
{
    switch (e.target.getAttribute("value"))
    {
        case "Save":
                var table = document.getElementById("main-table");
                var selectionsObject = {"selections":[]};
                for (let rowIndex = 0; table.children[0].childElementCount > rowIndex; rowIndex++)
                {
                    var row = table.children[0].children[rowIndex];
                    if (row.getAttribute("id") != null)
                    {
                        let name = row.children[0].children[0].value;
                        let url = row.children[1].children[0].value;
                        selectionsObject["selections"].push({"name":name,"url":url})
                        browser.storage.local.set({selectionsObject}).then(null);
                    }
                }
            browser.contextMenus.remove("quick-link").then(()=>{});
            browser.contextMenus.create(
            {
                id: "quick-link",
                title: "Quick Link",
                contexts: ["selection"]
            });
            browser.storage.local.get().then((data) =>
            {
                for (index in data["selectionsObject"]["selections"])
                {
                    browser.contextMenus.create(
                    {
                        parentId: "quick-link",
                        title: data["selectionsObject"]["selections"][index]["name"],
                        id: data["selectionsObject"]["selections"][index]["url"],
                    });
                }
            });

        case "Add Field":
            if (document.getElementById("empty") === null)
            {
                var table = document.getElementById("main-table");
                var row = table.insertRow();
                row.id = "empty";

                var nameInputCell = row.insertCell();
                var urlInputCell = row.insertCell();
                var removeCellButton = row.insertCell();

                var nameInput = document.createElement("input");
                nameInput.setAttribute('type',"text");
                nameInput.setAttribute('name',"name");

                var urlInput = document.createElement("input");
                urlInput.setAttribute('type',"text");
                urlInput.setAttribute('name',"url");

                var removeButton = document.createElement("input");
                removeButton.setAttribute('type',"button");
                removeButton.setAttribute('value',"Remove");

                nameInputCell.appendChild(nameInput);
                urlInputCell.appendChild(urlInput);
                removeCellButton.appendChild(removeButton);

                document.body.appendChild(table);
            }

        case "Remove":
            var table = e.target.parentElement.parentElement.parentElement;
            table.removeChild(e.target.parentElement.parentElement);
    }
});